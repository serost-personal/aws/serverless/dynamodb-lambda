import json


def lambda_handler(event, context):
    for record in event["Records"]:
        log_dynamodb_record(record)


def log_dynamodb_record(record):
    print(
        f"[{record['eventID']}] {record['eventName']}: {json.dumps(record['dynamodb'])}"
    )


# on when testing on local machine
if __name__ == "__main__":
    import ddb_stream_event

    # https://docs.aws.amazon.com/lambda/latest/dg/with-ddb-example.html
    lambda_handler(ddb_stream_event.event, None)
