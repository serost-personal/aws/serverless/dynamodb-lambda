import boto3
import os

# Create a resource service client by name using the default session.
dynamodb = boto3.resource("dynamodb")
table_name = os.environ.get("DYNAMODB_TABLE", "UserUsageCount")


def lambda_handler(event, context):
    # read user counter
    requester = event["user"]

    table = dynamodb.Table(table_name)

    counter: int

    response = table.get_item(Key={"user": requester})
    if "Item" in response:
        counter = int(response["Item"]["visit_counter"]) + 1
    else:
        counter = 1  # first time logging in

    # write counter back to database
    table.put_item(Item={"user": requester, "visit_counter": counter})

    return {
        "body": {
            "visit_counter": counter,
        },
    }


# on when testing on local machine
if __name__ == "__main__":
    event = {
        "user": "test",
    }
    print(lambda_handler(event, None))
